.. :changelog:

History
=======


0.1.0 (2014-04-30)
------------------
Creation

1.0.0 (2015-12-15)
------------------
Based on ltfat commit 0f9c83d96b (version 2.1.0)
