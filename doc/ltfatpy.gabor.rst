ltfatpy.gabor package - Gabor analysis
**************************************


Basic Time/Frequency analysis
=============================

s0norm
------

.. automodule:: ltfatpy.gabor.s0norm
    :members:
    :undoc-members:
    :show-inheritance:

Gabor systems
=============

dgt
---

.. automodule:: ltfatpy.gabor.dgt
    :members:
    :undoc-members:
    :show-inheritance:

idgt
----

.. automodule:: ltfatpy.gabor.idgt
    :members:
    :undoc-members:
    :show-inheritance:

dgtreal
-------

.. automodule:: ltfatpy.gabor.dgtreal
    :members:
    :undoc-members:
    :show-inheritance:

idgtreal
--------

.. automodule:: ltfatpy.gabor.idgtreal
    :members:
    :undoc-members:
    :show-inheritance:
    
dgtlength
---------

.. automodule:: ltfatpy.gabor.dgtlength
    :members:
    :undoc-members:
    :show-inheritance:

gabwin
------

.. automodule:: ltfatpy.gabor.gabwin
    :members:
    :undoc-members:
    :show-inheritance:

Reconstructing windows
======================

gabdual
-------

.. automodule:: ltfatpy.gabor.gabdual
    :members:
    :undoc-members:
    :show-inheritance:

gabtight
--------

.. automodule:: ltfatpy.gabor.gabtight
    :members:
    :undoc-members:
    :show-inheritance:

Conditions numbers
==================

gabframediag
------------

.. automodule:: ltfatpy.gabor.gabframediag
    :members:
    :undoc-members:
    :show-inheritance:

Phase gradient methods and reassignment
=======================================

gabphasegrad
------------

.. automodule:: ltfatpy.gabor.gabphasegrad
    :members:
    :undoc-members:
    :show-inheritance:


Phase reconstruction
====================

Phase conversions
=================

phaselock
---------

.. automodule:: ltfatpy.gabor.phaselock
    :members:
    :undoc-members:
    :show-inheritance:

phaseunlock
-----------

.. automodule:: ltfatpy.gabor.phaseunlock
    :members:
    :undoc-members:
    :show-inheritance:

Plots
=====

gabimagepars
------------

.. automodule:: ltfatpy.gabor.gabimagepars
    :members:
    :undoc-members:
    :show-inheritance:

instfreqplot
------------

.. automodule:: ltfatpy.gabor.instfreqplot
    :members:
    :undoc-members:
    :show-inheritance:

phaseplot
---------

.. automodule:: ltfatpy.gabor.phaseplot
    :members:
    :undoc-members:
    :show-inheritance:

plotdgt
-------

.. automodule:: ltfatpy.gabor.plotdgt
    :members:
    :undoc-members:
    :show-inheritance:

plotdgtreal
-----------

.. automodule:: ltfatpy.gabor.plotdgtreal
    :members:
    :undoc-members:
    :show-inheritance:

sgram
-----

.. automodule:: ltfatpy.gabor.sgram
    :members:
    :undoc-members:
    :show-inheritance:

tfplot
------

.. automodule:: ltfatpy.gabor.tfplot
    :members:
    :undoc-members:
    :show-inheritance:

..
	Module contents
	===============
	
	.. automodule:: ltfatpy.gabor
	    :members:
	    :undoc-members:
	    :show-inheritance:
