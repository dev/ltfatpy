.. ltfatpy documentation master file, created by
   sphinx-quickstart on Mon Jun 15 10:38:39 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ltfatpy documentation
=====================

.. automodule:: ltfatpy

Contents:

.. toctree::
    :maxdepth: 2
	
    install
    ltfatpy
    copyright
    zzz_bibliography
	
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

