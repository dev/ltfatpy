The **ltfatpy** package is a partial Python port of the
`Large Time/Frequency Analysis Toolbox (LTFAT)
<http://ltfat.sourceforge.net/>`_, a MATLAB®/Octave toolbox for working with
time-frequency analysis and synthesis.

It is intended both as an educational and a computational tool.

The package provides a large number of linear transforms including Gabor
transforms along with routines for constructing windows (filter prototypes)
and routines for manipulating coefficients.

The original LTFAT Toolbox for MATLAB®/Octave is developed at
`CAHR <http://www.dtu.dk/centre/cahr/English.aspx>`_, Technical
University of Denmark, `ARI <http://www.kfs.oeaw.ac.at>`_, Austrian Academy
of Sciences and `I2M <http://www.i2m.univ-amu.fr>`__, Aix-Marseille Université.

The Python port is developed at
`LabEx Archimède <http://labex-archimede.univ-amu.fr/>`_, as a
`LIF <http://www.lif.univ-mrs.fr/>`_ (now `LIS <http://www.lis-lab.fr/>`_)
and I2M project, Aix-Marseille Université.

This package, as well as the original LTFAT toolbox, is Free software, released
under the GNU General Public License (GPLv3).

The latest version of **ltfatpy** can be downloaded from the following
`PyPI page <https://pypi.python.org/pypi/ltfatpy>`_.

The **documentation** is available 
`here <http://dev.pages.lis-lab.fr/ltfatpy/>`_.

The development is done in this
`GitLab project <https://gitlab.lis-lab.fr/dev/ltfatpy>`_, which provides
the git repository managing the **source code** and where issues can be
reported.

